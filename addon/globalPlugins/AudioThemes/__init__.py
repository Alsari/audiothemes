# This file is using the following encoding: utf-8

# Audio Themes
# Copyright(C) 2012-2014 Alsari ibnomer2011@hotmail.com
 
import os
import wx

import globalPluginHandler
import appModuleHandler
import api
import gui
import controlTypes
import ui
import globalVars

import interface
import utils

from logHandler import log
from gui import messageBox as msg
from utils import *

import addonHandler
addonHandler.initTranslation()

class GlobalPlugin(globalPluginHandler.GlobalPlugin):

	def __init__(self):
		super(globalPluginHandler.GlobalPlugin, self).__init__()
		self.allowedApps = [u'firefox', u'iexplore', u'chrome', u'opera']
		self.themesMenu = wx.Menu()
		self.NTMitem = self.themesMenu.Append(wx.ID_ANY, 
		# Translators: the label of the menu item to open the audio themes manager dialog.
		_("Audio Themes &Manager..."), 
		# Translators: the tooltip text of the menu item that opens audio themes manager dialog
		_("Manage themes")
		)
		gui.mainFrame.sysTrayIcon.Bind(wx.EVT_MENU, lambda evt: interface.activate(interface.ThemesDialog), self.NTMitem)
		self.editorItem = self.themesMenu.Append(wx.ID_ANY, 
		# Translators: The label of the menu item that opens the audio themes editor dialog
		_("Audio Themes &Editor..."), 
		# Translators: The tooltip of the menu item that opens the audio themes editor dialog
		_("Create and/or edit the current theme"))
		self.helpItem = self.themesMenu.Append(wx.ID_ANY, 
		# Translators: The label of the menu item that opens the help document for this add-on
		_("&Help"),
		# Translators: The tooltip text of the help menu item
		_("Show Help Content")
		)
		gui.mainFrame.sysTrayIcon.Bind(wx.EVT_MENU, self.onHelp, self.helpItem)
		gui.mainFrame.sysTrayIcon.Bind(wx.EVT_MENU, lambda evt: interface.activate(interface.ActionDialog), self.editorItem)
		self.submenu_item = gui.mainFrame.sysTrayIcon.menu.InsertMenu(2, wx.ID_ANY, 
		# Translators: The label for this add-on's  menu
		_("&Audio Themes"), 
		self.themesMenu)
		utils.setupConfig()
		self.sounds = {}
		self.postInit()

	def postInit(self):
		if utils.conf['using'] != interface.NO_THEME_MSG:
			initialize_camlorn_audio()
			self.sounds = createSoundObjects()

	def terminate(self):
		if not self.submenu_item: return
		try:
			gui.mainFrame.sysTrayIcon.menu.RemoveItem(self.submenu_item)
		except wx.PyDeadObjectError:
			pass

	def onHelp(self, evt):
		globalVars.themesHelpFunc()

	def event_gainFocus(self, obj, nextHandler):
		f = ''
		if 16384 in obj.states:
			f = 'protected.wav'
		elif getLocation(obj):
			f = getLocation(obj)
		else:
			f = str(obj.role) + '.wav'
		try:
			play3D(self.sounds[f], obj)
		except:
			pass
		nextHandler()

	def event_becomeNavigatorObject(self, obj, nextHandler):
		self.event_gainFocus(obj, nextHandler)
		nextHandler()

	def event_show(self, obj, nextHandler):
		if obj.role == controlTypes.ROLE_HELPBALLOON:
			play3D(self.sounds["balloon.wav"], obj)
		nextHandler()

	def event_documentLoadComplete(self, obj, nextHandler):
		if appModuleHandler.getAppNameFromProcessID(obj.processID) in self.allowedApps:
			play3D(self.sounds["loaded.wav"], obj)
		nextHandler()


