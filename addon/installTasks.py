# installTasks.py

import os
import shutil
import config

def onInstall():
	""" 
	Moves The Default Themes To desired dir
	"""
	themesFolder = os.path.join(os.path.dirname(__file__), 'Themes')
	try:
		shutil.move(themesFolder, config.getUserDefaultConfigPath())
	except:
		pass

def onUninstall():
	"""
	remove add-on related file which can not be handle with nvda's add-on manager
	configpath\theem.ntm
	configpath\theems\
	"""
	configPath = config.getUserDefaultConfigPath()
	themesPath = os.path.join(configPath, "Themes")
	iniPath = os.path.join(configPath, "Themes.ini")
	if os.path.exists(themesPath):
		try:
			shutil.rmtree(themesPath)
		except:
			pass
	if os.path.exists(iniPath):
		try:
			os.remove(iniPath)
		except:
			pass
