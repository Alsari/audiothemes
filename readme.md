﻿## Audio Themes Add-on help ##


## Introduction ##
Audio themes is an add-on for NVDA Screen reader, that add a little twist for your navigation. for instance, you will hear a special and unique sound when NVDA's Focus moves to a new object: such as buttons, edit fields, and checkboxes. The sound will scale it self to give you a bird eye of view about object location, that is why we call it 3D.

##  FAQ ##
## How can I Select my  prefered Audio Theme? ##
After installing the Add-on, you can select your prefered Audio theme, by opening the nvda menu, then arrow down to "Audio Themes" menu item, then open the audio themes manager from the submenu. a window will open, showing all your installed themes. when you press enter the currently focused theme will become the active one after you restart NVDA.

## How can I add a new audio theme? ##
From the Audio Themes Manager, you can Add new audio themes, just press the Add button, and browse to your Audio theme package with the extention of ntm. after that you can make it active by pressing enter on it's entry.

## How can I create a new audio theme? ##
 The process is straightforward, just open the Audio Themes Editor, press the Create new audio theme button, then enter the name for your new audio theme package. after that the Create new theme wizered will be shown with an intuitive  interface. here is a  briefe explanation:
- Press the Base Path button to browse to the folder that contains wave files you want to use in your theme. note: you do not need to change the names of your files.
- After Selecting the base Path, the list will show all wave files found on that directory.
- as you arrow through list items, ademo of the selected audio file will be played back to you.
- From the combobox Select the object for which you want to assign the selected audio file.
- Press Assign button. Repeet this  for all your sounds and objects. note: there is special patterns you can assign audio files to them, such as: Last item, web page loaded, and more. you can find all of them on the combobox.
- When you Satisfied with your work, press package audio theme button to have your audio theme created and placed in the base path you have selected.
- You can install your theme and use it localy, or upload and share it.
- Share your Audio theme and show the world your talent.

## Contact Developer ##

You can contact me using one of these ways:
* Twitter: @mush42
* Email: ibnomer2011@hotmail.com
